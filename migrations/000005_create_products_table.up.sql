CREATE TABLE IF NOT EXISTS products(
  id VARCHAR(36) PRIMARY KEY,
  product_name VARCHAR(60) NOT NULL,
  product_price REAL NOT NULL,
  product_day REAL NOT NULL
);