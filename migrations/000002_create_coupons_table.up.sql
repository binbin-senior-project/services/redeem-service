CREATE TYPE coupon_status AS ENUM ('pending', 'active', 'rejected', 'expired');

CREATE TABLE IF NOT EXISTS coupons(
  id VARCHAR(36) PRIMARY KEY,
  coupon_name VARCHAR (255) NOT NULL,
  coupon_description VARCHAR (255) NOT NULL,
  coupon_condition VARCHAR (255) NOT NULL,
  coupon_point REAL NOT NULL default 10,
  coupon_photo VARCHAR (255) NOT NULL,
  coupon_expire TIMESTAMP NOT NULL,
  coupon_status coupon_status DEFAULT 'pending',
  category_id VARCHAR (36) NOT NULL,
  store_id VARCHAR (36) NOT NULL,
  user_id VARCHAR (36) NOT NULL,
  created_at TIMESTAMP default CURRENT_TIMESTAMP NOT NULL
);
