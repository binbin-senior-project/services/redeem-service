CREATE TABLE IF NOT EXISTS favorites(
  user_id VARCHAR(36),
  coupon_id VARCHAR(36),
  PRIMARY KEY (user_id, coupon_id)
);