package domain

import "time"

// Boost struct
type Boost struct {
	ID        string
	CouponID  string
	ProductID string
}

// Coupon struct
type Coupon struct {
	ID          string
	Name        string
	Photo       string
	Description string
	Point       float64
	Condition   string
	Status      string
	Expire      time.Time
	CreatedAt   time.Time
	CategoryID  string
	StoreID     string
	UserID      string
}

// Favorite struct
type Favorite struct {
	UserID   string
	CouponID string
}

// Product struct
type Product struct {
	ID    string
	Name  string
	Price float64
	Day   int
}

// Redeem struct
type Redeem struct {
	ID        string
	CouponID  string
	UserID    string
	CreatedAt time.Time
}

// Category struct
type Category struct {
	ID   string
	Name string
}

// User struct
type User struct {
	ID    string
	Point float64
}
