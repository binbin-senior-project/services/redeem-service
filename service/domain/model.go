package domain

// BoostItem struct
type BoostItem struct {
	ID          string
	CouponName  string
	ProductName string
}
