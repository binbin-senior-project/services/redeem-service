package domain

// UserCreatedEvent struct
type UserCreatedEvent struct {
	UserID string
}

// PointUpdatedEvent struct
type PointUpdatedEvent struct {
	UserID string
	Point  float64
}

// UserTransactionCreatedEvent struct
type UserTransactionCreatedEvent struct {
	UserID      string
	Type        string
	Point       float64
	Description string
}

// StoreTransactionCreatedEvent struct
type StoreTransactionCreatedEvent struct {
	StoreID     string
	Description string
}

// NotificationCreatedEvent struct
type NotificationCreatedEvent struct {
	UserID string
	Title  string
	Body   string
}
