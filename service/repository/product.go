package repository

import (
	"database/sql"

	"github.com/google/uuid"
	dm "gitlab.com/mangbinbin/services/redeem-service/service/domain"
)

// IProductRepository interface
type IProductRepository interface {
	Store(product dm.Product) error
	Fetch(offset int, limit int) ([]dm.Product, error)
	GetByID(id string) (dm.Product, error)
	Update(product dm.Product) error
	Delete(id string) error
}

// ProductRepository is an object
type ProductRepository struct {
	db *sql.DB
}

// NewProductRepository is an inital method for TrashRepository struct
// Called by Client (main.go)
func NewProductRepository(db *sql.DB) IProductRepository {
	return &ProductRepository{
		db: db,
	}
}

// Store Method
func (r *ProductRepository) Store(product dm.Product) error {

	query := `
		INSERT INTO products (id, product_name, product_price, product_day)
		VALUES ($1, $2, $3, $4)
	`

	product.ID = uuid.New().String()

	_, err := r.db.Query(query, product.ID, product.Name, product.Price, product.Day)

	return err
}

// Fetch Method
func (r *ProductRepository) Fetch(offset int, limit int) ([]dm.Product, error) {

	query := `
		SELECT id, product_name, product_price, product_day
		FROM products OFFSET $1 LIMIT $2
	`

	rows, err := r.db.Query(query, offset, limit)

	if err != nil {
		return nil, err
	}

	products := make([]dm.Product, 0)

	for rows.Next() {
		product := dm.Product{}

		err = rows.Scan(
			&product.ID,
			&product.Name,
			&product.Price,
			&product.Day,
		)

		if err != nil {
			return nil, err
		}

		products = append(products, product)
	}

	return products, nil
}

// GetByID Method
func (r *ProductRepository) GetByID(id string) (dm.Product, error) {
	query := `
		SELECT id, product_name, product_price, product_day
		FROM products WHERE id = $1
	`

	row := r.db.QueryRow(query, id)

	product := dm.Product{}

	err := row.Scan(
		&product.ID,
		&product.Name,
		&product.Price,
		&product.Day,
	)

	if err != nil {
		return dm.Product{}, err
	}

	return product, nil
}

// Update Method
func (r *ProductRepository) Update(product dm.Product) error {

	query := `
		UPDATE products SET product_name=$2, product_price=$3, product_day=$4 WHERE ID = $1
	`

	_, err := r.db.Query(query, product.ID, product.Name, product.Price, product.Day)

	if err != nil {
		return err
	}

	return nil
}

// Delete Method
func (r *ProductRepository) Delete(id string) error {

	query := `
		DELETE FROM products WHERE id = $1
	`

	_, err := r.db.Query(query, id)

	if err != nil {
		return err
	}

	return nil
}
