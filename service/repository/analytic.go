package repository

import (
	"database/sql"
)

// IAnalyticRepository interface
type IAnalyticRepository interface {
	GetNewCouponToday() (int, error)
	GetRedeems(couponID string) (int, error)
}

// AnalyticRepository is an object
type AnalyticRepository struct {
	db *sql.DB
}

// NewAnalyticRepository is an inital method for TrashRepository struct
// Called by Client (main.go)
func NewAnalyticRepository(db *sql.DB) IAnalyticRepository {
	return &AnalyticRepository{
		db: db,
	}
}

// GetNewCouponToday method
func (r *AnalyticRepository) GetNewCouponToday() (int, error) {
	query := `SELECT COUNT(*) FROM coupons WHERE DATE("created_at") = current_date`

	row := r.db.QueryRow(query)

	var count int

	err := row.Scan(
		&count,
	)

	if err != nil {
		return 0, err
	}

	return count, nil
}

// GetRedeems method
func (r *AnalyticRepository) GetRedeems(couponID string) (int, error) {

	query := `SELECT COUNT(*) FROM redeems WHERE coupon_id = $1`

	row := r.db.QueryRow(query, couponID)

	var count int

	err := row.Scan(
		&count,
	)

	if err != nil {
		return 0, err
	}

	return count, nil
}
