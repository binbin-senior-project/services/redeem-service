package repository

type RegisterRepository struct {
	Analytic IAnalyticRepository
	Boost    IBoostRepository
	Category ICategoryRepository
	Coupon   ICouponRepository
	Favorite IFavoriteRepository
	Product  IProductRepository
	Redeem   IRedeemRepository
	User     IUserRepository
}
