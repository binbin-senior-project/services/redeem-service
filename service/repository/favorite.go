package repository

import (
	"database/sql"

	dm "gitlab.com/mangbinbin/services/redeem-service/service/domain"
)

// IFavoriteRepository interface
type IFavoriteRepository interface {
	Store(userID string, couponID string) error
	GetByUserID(userID string, offset int, limit int) ([]dm.Favorite, error)
	GetByUserIDAndCouponID(userID string, couponID string) (dm.Favorite, error)
	Delete(userID string, couponID string) error
}

// FavoriteRepository is an object
type FavoriteRepository struct {
	db *sql.DB
}

// NewFavoriteRepository is an inital method for TrashRepository struct
// Called by Client (main.go)
func NewFavoriteRepository(db *sql.DB) IFavoriteRepository {
	return &FavoriteRepository{
		db: db,
	}
}

// Store method
func (r *FavoriteRepository) Store(userID string, couponID string) error {

	query := `
		INSERT INTO favorites (user_id, coupon_id)
		VALUES ($1, $2)
	`

	_, err := r.db.Query(
		query,
		userID,
		couponID,
	)

	return err
}

// GetByUserID method
func (r *FavoriteRepository) GetByUserID(userID string, offset int, limit int) ([]dm.Favorite, error) {

	query := `
		SELECT user_id, coupon_id
		FROM favorites WHERE user_id = $1 OFFSET $2 LIMIT $3
	`

	rows, err := r.db.Query(query, userID, offset, limit)

	if err != nil {
		return nil, err
	}

	favorites := make([]dm.Favorite, 0)

	for rows.Next() {
		favorite := dm.Favorite{}

		err = rows.Scan(
			&favorite.UserID,
			&favorite.CouponID,
		)

		if err != nil {
			return nil, err
		}

		favorites = append(favorites, favorite)
	}

	return favorites, nil
}

// GetByUserIDAndCouponID method
func (r *FavoriteRepository) GetByUserIDAndCouponID(userID string, couponID string) (dm.Favorite, error) {

	query := `
		SELECT user_id, coupon_id
		FROM favorites WHERE user_id = $1 AND coupon_id = $2
	`

	row := r.db.QueryRow(query, userID, couponID)

	favorite := dm.Favorite{}

	err := row.Scan(
		&favorite.UserID,
		&favorite.CouponID,
	)

	if err != nil {
		return dm.Favorite{}, err
	}

	return favorite, nil
}

// Delete method
func (r *FavoriteRepository) Delete(userID string, couponID string) error {

	query := `
		DELETE FROM favorites 
		WHERE user_id = $1 and coupon_id = $2
	`

	_, err := r.db.Query(query, userID, couponID)

	return err
}
