package repository

import (
	"database/sql"

	"github.com/google/uuid"
	dm "gitlab.com/mangbinbin/services/redeem-service/service/domain"
)

// IBoostRepository interface
type IBoostRepository interface {
	Store(couponID string, productID string) error
	Fetch(offset int, limit int) ([]dm.Boost, error)
	GetByID(id string) (dm.Boost, error)
	Delete(id string) error
}

// BoostRepository is an object
type BoostRepository struct {
	db *sql.DB
}

// NewBoostRepository is an inital method
func NewBoostRepository(db *sql.DB) IBoostRepository {
	return &BoostRepository{
		db: db,
	}
}

// Store method
func (r *BoostRepository) Store(couponID string, productID string) error {

	query := `
	INSERT INTO boosts (id, coupon_id, product_id)
	VALUES ($1, $2, $3)
`

	_, err := r.db.Query(
		query,
		uuid.New().String(),
		couponID,
		productID,
	)

	return err
}

// Fetch method
func (r *BoostRepository) Fetch(offset int, limit int) ([]dm.Boost, error) {

	query := `
		SELECT id, coupon_id, product_id
		FROM boosts OFFSET $1 LIMIT $2
	`

	rows, err := r.db.Query(query, offset, limit)

	if err != nil {
		return nil, err
	}

	boosts := make([]dm.Boost, 0)

	for rows.Next() {
		boost := dm.Boost{}

		err = rows.Scan(
			&boost.ID,
			&boost.CouponID,
			&boost.ProductID,
		)

		if err != nil {
			return nil, err
		}

		boosts = append(boosts, boost)
	}

	return boosts, nil
}

// GetByID method
func (r *BoostRepository) GetByID(id string) (dm.Boost, error) {

	query := `SELECT id, coupon_id, product_id FROM boosts WHERE id = $1`

	row := r.db.QueryRow(query, id)

	boost := dm.Boost{}

	err := row.Scan(
		&boost.ID,
		&boost.CouponID,
		&boost.ProductID,
	)

	if err != nil {
		return dm.Boost{}, err
	}

	return boost, nil
}

// Delete method
func (r *BoostRepository) Delete(id string) error {

	query := `DELETE FROM boosts WHERE id = $1`

	_, err := r.db.Query(query, id)

	return err
}
