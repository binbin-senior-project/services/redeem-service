package repository

import (
	"database/sql"
	"time"

	"github.com/google/uuid"
	dm "gitlab.com/mangbinbin/services/redeem-service/service/domain"
)

// ICouponRepository interface
type ICouponRepository interface {
	Store(coupon dm.Coupon) error
	Fetch(offset int, limit int) ([]dm.Coupon, error)
	GetByKeyword(keyword string, offset int, limit int) ([]dm.Coupon, error)
	GetByCategoryID(categoryID string, offset int, limit int) ([]dm.Coupon, error)
	GetByStoreID(storeID string, offset int, limit int) ([]dm.Coupon, error)
	GetByStatus(status string, offset int, limit int) ([]dm.Coupon, error)
	GetByStoreIDAndStatus(storeID string, status string, offset int, limit int) ([]dm.Coupon, error)
	GetByID(id string) (dm.Coupon, error)
	Update(coupon dm.Coupon) error
	UpdateByKey(id string, key string, value string) error
	UpdateExpire(id string, expire time.Time) error
	Delete(id string) error
}

// CouponRepository is an object
type CouponRepository struct {
	db *sql.DB
}

// NewCouponRepository is an inital method for TrashRepository struct
// Called by Client (main.go)
func NewCouponRepository(db *sql.DB) ICouponRepository {
	return &CouponRepository{
		db: db,
	}
}

// Store method
func (r *CouponRepository) Store(coupon dm.Coupon) error {

	query := `
		INSERT INTO coupons (id, coupon_name, coupon_photo, coupon_description, coupon_point, coupon_condition, coupon_expire, category_id, store_id, user_id)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
	`

	coupon.ID = uuid.New().String()

	_, err := r.db.Query(
		query,
		coupon.ID,
		coupon.Name,
		coupon.Photo,
		coupon.Description,
		coupon.Point,
		coupon.Condition,
		coupon.Expire,
		coupon.CategoryID,
		coupon.StoreID,
		coupon.UserID,
	)

	return err
}

// Fetch method
func (r *CouponRepository) Fetch(offset int, limit int) ([]dm.Coupon, error) {

	query := `
		SELECT id, coupon_name, coupon_photo, coupon_description, coupon_point, coupon_condition, coupon_status, coupon_expire, created_at, category_id, store_id, user_id
		FROM coupons ORDER BY created_at DESC OFFSET $1 LIMIT $2
	`

	rows, err := r.db.Query(query, offset, limit)

	if err != nil {
		return nil, err
	}

	coupons := make([]dm.Coupon, 0)

	for rows.Next() {
		coupon := dm.Coupon{}

		err = rows.Scan(
			&coupon.ID,
			&coupon.Name,
			&coupon.Photo,
			&coupon.Description,
			&coupon.Point,
			&coupon.Condition,
			&coupon.Status,
			&coupon.Expire,
			&coupon.CreatedAt,
			&coupon.CategoryID,
			&coupon.StoreID,
			&coupon.UserID,
		)

		if err != nil {
			return nil, err
		}

		coupons = append(coupons, coupon)
	}

	return coupons, nil
}

// GetByKeyword method
func (r *CouponRepository) GetByKeyword(keyword string, offset int, limit int) ([]dm.Coupon, error) {

	query := `
		SELECT id, coupon_name, coupon_photo, coupon_description, coupon_point, coupon_condition, coupon_status, coupon_expire, created_at, category_id, store_id, user_id
		FROM coupons WHERE coupon_name LIKE $1 ORDER BY created_at DESC OFFSET $2 LIMIT $3
	`

	rows, err := r.db.Query(query, "%"+keyword+"%", offset, limit)

	if err != nil {
		return nil, err
	}

	coupons := make([]dm.Coupon, 0)

	for rows.Next() {
		coupon := dm.Coupon{}

		err = rows.Scan(
			&coupon.ID,
			&coupon.Name,
			&coupon.Photo,
			&coupon.Description,
			&coupon.Point,
			&coupon.Condition,
			&coupon.Status,
			&coupon.Expire,
			&coupon.CreatedAt,
			&coupon.CategoryID,
			&coupon.StoreID,
			&coupon.UserID,
		)

		if err != nil {
			return nil, err
		}

		coupons = append(coupons, coupon)
	}

	return coupons, nil
}

// GetByCategoryID method
func (r *CouponRepository) GetByCategoryID(categoryID string, offset int, limit int) ([]dm.Coupon, error) {

	query := `
	SELECT id, coupon_name, coupon_photo, coupon_description, coupon_point, coupon_condition, coupon_status, coupon_expire, created_at, category_id, store_id, user_id
	FROM coupons WHERE category_id  = $1 ORDER BY created_at DESC OFFSET $2 LIMIT $3
`

	rows, err := r.db.Query(query, categoryID, offset, limit)

	if err != nil {
		return nil, err
	}

	coupons := make([]dm.Coupon, 0)

	for rows.Next() {
		coupon := dm.Coupon{}

		err = rows.Scan(
			&coupon.ID,
			&coupon.Name,
			&coupon.Photo,
			&coupon.Description,
			&coupon.Point,
			&coupon.Condition,
			&coupon.Status,
			&coupon.Expire,
			&coupon.CreatedAt,
			&coupon.CategoryID,
			&coupon.StoreID,
			&coupon.UserID,
		)

		if err != nil {
			return nil, err
		}

		coupons = append(coupons, coupon)
	}

	return coupons, nil
}

// GetByStoreID method
func (r *CouponRepository) GetByStoreID(storeID string, offset int, limit int) ([]dm.Coupon, error) {

	query := `
		SELECT id, coupon_name, coupon_photo, coupon_description, coupon_point, coupon_condition, coupon_status, coupon_expire, created_at, category_id, store_id, user_id
		FROM coupons WHERE store_id = $1 ORDER BY created_at DESC OFFSET $2 LIMIT $3
	`

	rows, err := r.db.Query(query, storeID, offset, limit)

	if err != nil {
		return nil, err
	}

	coupons := make([]dm.Coupon, 0)

	for rows.Next() {
		coupon := dm.Coupon{}

		err = rows.Scan(
			&coupon.ID,
			&coupon.Name,
			&coupon.Photo,
			&coupon.Description,
			&coupon.Point,
			&coupon.Condition,
			&coupon.Status,
			&coupon.Expire,
			&coupon.CreatedAt,
			&coupon.CategoryID,
			&coupon.StoreID,
			&coupon.UserID,
		)

		if err != nil {
			return nil, err
		}

		coupons = append(coupons, coupon)
	}

	return coupons, nil
}

// GetByStatus method
func (r *CouponRepository) GetByStatus(status string, offset int, limit int) ([]dm.Coupon, error) {

	query := `
		SELECT id, coupon_name, coupon_photo, coupon_description, coupon_point, coupon_condition, coupon_status, coupon_expire, created_at, category_id, store_id, user_id
		FROM coupons WHERE coupon_status = $1 ORDER BY created_at DESC OFFSET $2 LIMIT $3
	`

	rows, err := r.db.Query(query, status, offset, limit)

	if err != nil {
		return nil, err
	}

	coupons := make([]dm.Coupon, 0)

	for rows.Next() {
		coupon := dm.Coupon{}

		err = rows.Scan(
			&coupon.ID,
			&coupon.Name,
			&coupon.Photo,
			&coupon.Description,
			&coupon.Point,
			&coupon.Condition,
			&coupon.Status,
			&coupon.Expire,
			&coupon.CreatedAt,
			&coupon.CategoryID,
			&coupon.StoreID,
			&coupon.UserID,
		)

		if err != nil {
			return nil, err
		}

		coupons = append(coupons, coupon)
	}

	return coupons, nil
}

// GetByStoreIDAndStatus method
func (r *CouponRepository) GetByStoreIDAndStatus(storeID string, status string, offset int, limit int) ([]dm.Coupon, error) {

	query := `
		SELECT id, coupon_name, coupon_photo, coupon_description, coupon_point, coupon_condition, coupon_status, coupon_expire, created_at, category_id, store_id, user_id
		FROM coupons WHERE store_id = $1 AND coupon_status = $2 ORDER BY created_at DESC OFFSET $3 LIMIT $4
	`

	rows, err := r.db.Query(query, storeID, status, offset, limit)

	if err != nil {
		return nil, err
	}

	coupons := make([]dm.Coupon, 0)

	for rows.Next() {
		coupon := dm.Coupon{}

		err = rows.Scan(
			&coupon.ID,
			&coupon.Name,
			&coupon.Photo,
			&coupon.Description,
			&coupon.Point,
			&coupon.Condition,
			&coupon.Status,
			&coupon.Expire,
			&coupon.CreatedAt,
			&coupon.CategoryID,
			&coupon.StoreID,
			&coupon.UserID,
		)

		if err != nil {
			return nil, err
		}

		coupons = append(coupons, coupon)
	}

	return coupons, nil
}

// GetByID method
func (r *CouponRepository) GetByID(id string) (dm.Coupon, error) {

	query := `
		SELECT id, coupon_name, coupon_photo, coupon_description, coupon_point, coupon_condition, coupon_status, coupon_expire, created_at, category_id, store_id, user_id
		FROM coupons WHERE id = $1
	`

	row := r.db.QueryRow(query, id)

	coupon := dm.Coupon{}

	err := row.Scan(
		&coupon.ID,
		&coupon.Name,
		&coupon.Photo,
		&coupon.Description,
		&coupon.Point,
		&coupon.Condition,
		&coupon.Status,
		&coupon.Expire,
		&coupon.CreatedAt,
		&coupon.CategoryID,
		&coupon.StoreID,
		&coupon.UserID,
	)

	if err != nil {
		return dm.Coupon{}, err
	}

	return coupon, nil
}

// Update method
func (r *CouponRepository) Update(coupon dm.Coupon) error {

	query := `
		UPDATE coupons SET coupon_name=$2, coupon_photo=$3, coupon_description=$4, coupon_point=$5, coupon_condition=$6, category_id=$7 WHERE id = $1
	`

	_, err := r.db.Query(
		query,
		&coupon.ID,
		&coupon.Name,
		&coupon.Photo,
		&coupon.Description,
		&coupon.Point,
		&coupon.Condition,
		&coupon.CategoryID,
	)

	if err != nil {
		return err
	}

	return nil
}

// UpdateByKey method
func (r *CouponRepository) UpdateByKey(id string, key string, value string) error {
	query := `UPDATE coupons SET ` + key + `=$2 WHERE id = $1`

	_, err := r.db.Query(query, id, value)

	return err
}

// UpdateExpire method
func (r *CouponRepository) UpdateExpire(id string, expire time.Time) error {
	query := `UPDATE coupons SET coupon_expire = $2 WHERE id = $1`

	_, err := r.db.Query(query, id, expire)

	return err
}

// Delete method
func (r *CouponRepository) Delete(id string) error {

	query := `
		DELETE FROM coupons WHERE id = $1
	`

	_, err := r.db.Query(query, id)

	return err
}
