package repository

import (
	"database/sql"

	"github.com/google/uuid"
	dm "gitlab.com/mangbinbin/services/redeem-service/service/domain"
)

// IRedeemRepository interface
type IRedeemRepository interface {
	Store(redeem dm.Redeem) error
	GetByCouponID(couponID string) (dm.Redeem, error)
}

// RedeemRepository is an object
type RedeemRepository struct {
	db *sql.DB
}

// NewRedeemRepository is an inital method for TrashRepository struct
// Called by Client (main.go)
func NewRedeemRepository(db *sql.DB) IRedeemRepository {
	return &RedeemRepository{
		db: db,
	}
}

// Store method
func (r *RedeemRepository) Store(redeem dm.Redeem) error {

	query := `
		INSERT INTO redeems (id, coupon_id, user_id)
		VALUES ($1, $2, $3)
	`

	redeem.ID = uuid.New().String()

	_, err := r.db.Query(query, redeem.ID, redeem.CouponID, redeem.UserID)

	return err
}

// GetByCouponID method
func (r *RedeemRepository) GetByCouponID(couponID string) (dm.Redeem, error) {
	query := `
		SELECT id, coupon_id, user_id, created_at
		FROM products WHERE coupon_id = $1
	`

	row := r.db.QueryRow(query, couponID)

	redeem := dm.Redeem{}

	err := row.Scan(
		&redeem.ID,
		&redeem.CouponID,
		&redeem.UserID,
		&redeem.CreatedAt,
	)

	if err != nil {
		return dm.Redeem{}, err
	}

	return redeem, nil
}
