package repository

import (
	"database/sql"

	dm "gitlab.com/mangbinbin/services/redeem-service/service/domain"
)

// IUserRepository interface
type IUserRepository interface {
	Store(user dm.User) error
	GetByID(id string) (dm.User, error)
	UpdateByKey(id string, key string, value string) error
}

// UserRepository is an object
type UserRepository struct {
	db *sql.DB
}

// NewUserRepository is an inital method
func NewUserRepository(db *sql.DB) IUserRepository {
	return &UserRepository{
		db: db,
	}
}

// Store method is a method
func (r *UserRepository) Store(user dm.User) error {

	query := `
		INSERT INTO users (id)
		VALUES ($1)
	`

	_, err := r.db.Query(
		query,
		user.ID,
	)

	return err
}

// GetByID method is a method
func (r *UserRepository) GetByID(id string) (dm.User, error) {

	query := `
		SELECT id, user_point FROM users WHERE id = $1
	`

	row := r.db.QueryRow(query, id)

	user := dm.User{}

	err := row.Scan(
		&user.ID,
		&user.Point,
	)

	if err != nil {
		return dm.User{}, err
	}

	return user, nil
}

// UpdateByKey is a method
func (r *UserRepository) UpdateByKey(id string, key string, value string) error {
	query := `UPDATE users SET ` + key + `=$2 WHERE id = $1`

	_, err := r.db.Query(query, id, value)

	return err
}
