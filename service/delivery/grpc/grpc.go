package grpc

import (
	"log"
	"net"
	"os"

	pb "gitlab.com/mangbinbin/services/redeem-service/service/delivery/grpc/proto"
	usecase "gitlab.com/mangbinbin/services/redeem-service/service/usecase"
	"google.golang.org/grpc"
)

// RedeemDelivery Holayy!
type RedeemDelivery struct {
	usecase usecase.RegisterUsecase
}

// NewRedeemDelivery method is an intial grpc package
// to start gRPC transportation
func NewRedeemDelivery(usecase usecase.RegisterUsecase) {
	// Register protobuf handler with RedeemgRPCDelievery struct
	delivery := &RedeemDelivery{
		usecase: usecase,
	}

	port := os.Getenv("SERVICE_PORT")

	// Listen out
	log.Println("Listen on port: " + port)

	// Create TCP Connection
	lis, err := net.Listen("tcp", ":"+port)

	// Detect connection error
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// Create GRPC service
	s := grpc.NewServer()

	// Register GRPC to protobuffer
	pb.RegisterRedeemServiceServer(s, delivery)

	// Register TCP connection to GRPC
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
