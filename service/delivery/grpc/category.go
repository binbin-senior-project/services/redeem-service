package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/redeem-service/service/delivery/grpc/proto"
	dm "gitlab.com/mangbinbin/services/redeem-service/service/domain"
)

// CreateCouponCategory method
func (d *RedeemDelivery) CreateCouponCategory(ctx context.Context, req *pb.CreateCouponCategoryRequest) (*pb.CreateCouponCategoryReply, error) {
	// Create Account
	err := d.usecase.Category.Store(dm.Category{Name: req.Name})

	if err != nil {
		return &pb.CreateCouponCategoryReply{Success: false}, err
	}

	return &pb.CreateCouponCategoryReply{Success: true}, nil
}

// GetCouponCategories method
func (d *RedeemDelivery) GetCouponCategories(ctx context.Context, req *pb.GetCouponCategoriesRequest) (*pb.GetCouponCategoriesReply, error) {
	// Create Account
	categorys, err := d.usecase.Category.Fetch(int(req.Offset), int(req.Limit))

	if err != nil {
		return &pb.GetCouponCategoriesReply{}, err
	}

	var categoryReplies = []*pb.CouponCategory{}

	for _, category := range categorys {
		categoryReplies = append(categoryReplies, &pb.CouponCategory{
			Id:   category.ID,
			Name: category.Name,
		})
	}

	rsp := &pb.GetCouponCategoriesReply{
		Categories: categoryReplies,
	}

	// No error in this case
	return rsp, nil
}

// GetCouponCategory method
func (d *RedeemDelivery) GetCouponCategory(ctx context.Context, req *pb.GetCouponCategoryRequest) (*pb.GetCouponCategoryReply, error) {
	// Create Account
	category, err := d.usecase.Category.GetByID(req.CategoryID)

	if err != nil {
		return &pb.GetCouponCategoryReply{}, err
	}

	// No error in this case
	return &pb.GetCouponCategoryReply{
		Category: &pb.CouponCategory{
			Id:   category.ID,
			Name: category.Name,
		},
	}, nil
}

// UpdateCouponCategory method
func (d *RedeemDelivery) UpdateCouponCategory(ctx context.Context, req *pb.UpdateCouponCategoryRequest) (*pb.UpdateCouponCategoryReply, error) {
	category := dm.Category{
		ID:   req.CategoryID,
		Name: req.Name,
	}

	err := d.usecase.Category.Update(category)

	if err != nil {
		return &pb.UpdateCouponCategoryReply{Success: false}, err
	}

	return &pb.UpdateCouponCategoryReply{Success: true}, nil
}

// DeleteCouponCategory method
func (d *RedeemDelivery) DeleteCouponCategory(ctx context.Context, req *pb.DeleteCouponCategoryRequest) (*pb.DeleteCouponCategoryReply, error) {
	// Create Account
	err := d.usecase.Category.Delete(req.CategoryID)

	if err != nil {
		return &pb.DeleteCouponCategoryReply{Success: false}, err
	}

	// No error in this case
	return &pb.DeleteCouponCategoryReply{Success: true}, nil
}
