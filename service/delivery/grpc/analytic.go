package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/redeem-service/service/delivery/grpc/proto"
)

// GetNewCouponToday method
func (d *RedeemDelivery) GetNewCouponToday(ctx context.Context, req *pb.GetNewCouponTodayRequest) (*pb.GetNewCouponTodayReply, error) {
	count, err := d.usecase.Analytic.GetNewCouponToday()

	if err != nil {
		return &pb.GetNewCouponTodayReply{}, err
	}

	return &pb.GetNewCouponTodayReply{
		Count: int32(count),
	}, nil
}

// GetRedeems method
func (d *RedeemDelivery) GetRedeems(ctx context.Context, req *pb.GetRedeemsRequest) (*pb.GetRedeemsReply, error) {
	// Create Account
	result, err := d.usecase.Analytic.GetRedeems(req.CouponID)

	if err != nil {
		return &pb.GetRedeemsReply{Count: 0}, err
	}

	// No error in this case
	return &pb.GetRedeemsReply{Count: int32(result)}, nil
}
