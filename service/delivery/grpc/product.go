package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/redeem-service/service/delivery/grpc/proto"
	dm "gitlab.com/mangbinbin/services/redeem-service/service/domain"
)

// CreateProduct method
func (d *RedeemDelivery) CreateProduct(ctx context.Context, req *pb.CreateProductRequest) (*pb.CreateProductReply, error) {

	err := d.usecase.Product.Store(dm.Product{
		Name:  req.Name,
		Price: req.Price,
		Day:   int(req.Day),
	})

	if err != nil {
		return &pb.CreateProductReply{Success: false}, err
	}

	// No error in this case
	return &pb.CreateProductReply{Success: true}, nil
}

// GetProducts method
func (d *RedeemDelivery) GetProducts(ctx context.Context, req *pb.GetProductsRequest) (*pb.GetProductsReply, error) {

	products, err := d.usecase.Product.Fetch(int(req.Offset), int(req.Limit))

	if err != nil {
		return &pb.GetProductsReply{}, err
	}

	var produuctReplies = []*pb.Product{}

	for _, product := range products {
		produuctReplies = append(produuctReplies, &pb.Product{
			Id:    product.ID,
			Name:  product.Name,
			Price: product.Price,
			Day:   int32(product.Day),
		})
	}

	rsp := &pb.GetProductsReply{
		Products: produuctReplies,
	}

	// No error in this case
	return rsp, nil
}

// GetProduct method
func (d *RedeemDelivery) GetProduct(ctx context.Context, req *pb.GetProductRequest) (*pb.GetProductReply, error) {

	product, err := d.usecase.Product.GetByID(req.ProductID)

	if err != nil {
		return &pb.GetProductReply{}, err
	}

	// No error in this case
	return &pb.GetProductReply{
		Product: &pb.Product{
			Id:    product.ID,
			Name:  product.Name,
			Price: product.Price,
			Day:   int32(product.Day),
		},
	}, nil
}

// UpdateProduct method
func (d *RedeemDelivery) UpdateProduct(ctx context.Context, req *pb.UpdateProductRequest) (*pb.UpdateProductReply, error) {

	err := d.usecase.Product.Update(dm.Product{
		ID:    req.ProductID,
		Name:  req.Name,
		Price: req.Price,
		Day:   int(req.Day),
	})

	if err != nil {
		return &pb.UpdateProductReply{Success: false}, err
	}

	// No error in this case
	return &pb.UpdateProductReply{Success: true}, nil
}

// DeleteProduct method
func (d *RedeemDelivery) DeleteProduct(ctx context.Context, req *pb.DeleteProductRequest) (*pb.DeleteProductReply, error) {

	err := d.usecase.Product.Delete(req.ProductID)

	if err != nil {
		return &pb.DeleteProductReply{Success: false}, err
	}

	// No error in this case
	return &pb.DeleteProductReply{Success: true}, nil
}
