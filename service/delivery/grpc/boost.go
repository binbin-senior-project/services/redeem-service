package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/redeem-service/service/delivery/grpc/proto"
)

// CreateBoost method
func (d *RedeemDelivery) CreateBoost(ctx context.Context, req *pb.CreateBoostRequest) (*pb.CreateBoostReply, error) {

	err := d.usecase.Boost.Store(req.CouponIDs, req.ProductID)

	if err != nil {
		return &pb.CreateBoostReply{Success: false}, err
	}

	return &pb.CreateBoostReply{Success: true}, nil
}

// GetBoosts method
func (d *RedeemDelivery) GetBoosts(ctx context.Context, req *pb.GetBoostsRequest) (*pb.GetBoostsReply, error) {

	boostItems, err := d.usecase.Boost.Fetch(int(req.Offset), int(req.Limit))

	if err != nil {
		return &pb.GetBoostsReply{}, err
	}

	var replies = []*pb.BoostItem{}

	for _, item := range boostItems {
		replies = append(replies, &pb.BoostItem{
			Id:          item.ID,
			CouponName:  item.CouponName,
			ProductName: item.ProductName,
		})
	}

	return &pb.GetBoostsReply{
		Boosts: replies,
	}, nil
}

// ApproveBoost method
func (d *RedeemDelivery) ApproveBoost(ctx context.Context, req *pb.ApproveBoostRequest) (*pb.ApproveBoostReply, error) {

	err := d.usecase.Boost.Approve(req.BoostID)

	if err != nil {
		return &pb.ApproveBoostReply{Success: false}, err
	}

	return &pb.ApproveBoostReply{Success: true}, nil
}

// RejectBoost method
func (d *RedeemDelivery) RejectBoost(ctx context.Context, req *pb.RejectBoostRequest) (*pb.RejectBoostReply, error) {

	err := d.usecase.Boost.Reject(req.BoostID)

	if err != nil {
		return &pb.RejectBoostReply{Success: false}, err
	}

	return &pb.RejectBoostReply{Success: true}, nil
}
