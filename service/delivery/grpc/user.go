package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/redeem-service/service/delivery/grpc/proto"
	dm "gitlab.com/mangbinbin/services/redeem-service/service/domain"
)

// RedeemCoupon method
func (d *RedeemDelivery) RedeemCoupon(ctx context.Context, req *pb.RedeemCouponRequest) (*pb.RedeemCouponReply, error) {

	err := d.usecase.User.Redeem(dm.Redeem{
		UserID:   req.UserID,
		CouponID: req.CouponID,
	})

	if err != nil {
		return &pb.RedeemCouponReply{Success: false}, err
	}

	return &pb.RedeemCouponReply{Success: true}, nil
}

// GetFavoriteCoupons method
func (d *RedeemDelivery) GetFavoriteCoupons(ctx context.Context, req *pb.GetFavoriteCouponsRequest) (*pb.GetFavoriteCouponsReply, error) {
	coupons, err := d.usecase.User.GetFavoritesByUserID(req.UserID, int(req.Offset), int(req.Limit))

	if err != nil {
		return &pb.GetFavoriteCouponsReply{}, err
	}

	var couponReplies = []*pb.Coupon{}

	for _, coupon := range coupons {
		couponReplies = append(couponReplies, &pb.Coupon{
			Id:          coupon.ID,
			Name:        coupon.Name,
			Photo:       coupon.Photo,
			Description: coupon.Description,
			Point:       coupon.Point,
			Condition:   coupon.Condition,
			Expire:      coupon.Expire.String(),
			CategoryID:  coupon.CategoryID,
			StoreID:     coupon.StoreID,
		})
	}

	rsp := &pb.GetFavoriteCouponsReply{
		Coupons: couponReplies,
	}

	// No error in this case
	return rsp, nil
}

// CheckFavoriteCoupon method
func (d *RedeemDelivery) CheckFavoriteCoupon(ctx context.Context, req *pb.CheckFavoriteCouponRequest) (*pb.CheckFavoriteCouponReply, error) {
	_, err := d.usecase.User.CheckFavoriteCoupon(req.UserID, req.CouponID)

	if err != nil {
		return &pb.CheckFavoriteCouponReply{Success: false}, nil
	}

	// No error in this case
	return &pb.CheckFavoriteCouponReply{Success: true}, nil
}

// FavoriteCoupon method
func (d *RedeemDelivery) FavoriteCoupon(ctx context.Context, req *pb.FavoriteCouponRequest) (*pb.FavoriteCouponReply, error) {
	err := d.usecase.User.FavoriteCoupon(req.UserID, req.CouponID)

	if err != nil {
		return &pb.FavoriteCouponReply{Success: false}, err
	}

	// No error in this case
	return &pb.FavoriteCouponReply{Success: true}, nil
}

// UnFavoriteCoupon method
func (d *RedeemDelivery) UnFavoriteCoupon(ctx context.Context, req *pb.UnFavoriteCouponRequest) (*pb.UnFavoriteCouponReply, error) {
	err := d.usecase.User.UnFavoriteCoupon(req.UserID, req.CouponID)

	if err != nil {
		return &pb.UnFavoriteCouponReply{Success: false}, err
	}

	// No error in this case
	return &pb.UnFavoriteCouponReply{Success: true}, nil
}
