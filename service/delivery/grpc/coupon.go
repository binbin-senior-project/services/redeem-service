package grpc

import (
	"context"
	"time"

	pb "gitlab.com/mangbinbin/services/redeem-service/service/delivery/grpc/proto"
	dm "gitlab.com/mangbinbin/services/redeem-service/service/domain"
)

// CreateCoupon method
func (d *RedeemDelivery) CreateCoupon(ctx context.Context, req *pb.CreateCouponRequest) (*pb.CreateCouponReply, error) {

	coupon := dm.Coupon{
		Name:        req.Name,
		Photo:       req.Photo,
		Description: req.Description,
		Point:       req.Point,
		CategoryID:  req.CategoryID,
		StoreID:     req.StoreID,
		UserID:      req.UserID,
		Condition:   req.Condition,
		Expire:      time.Now().Add(time.Hour * 24 * 3), // expire within next 3 days
	}

	err := d.usecase.Coupon.Store(coupon)

	if err != nil {
		return &pb.CreateCouponReply{Success: false}, err
	}

	// Notify to store
	// Code Here

	return &pb.CreateCouponReply{Success: true}, nil
}

// UpdateCoupon method
func (d *RedeemDelivery) UpdateCoupon(ctx context.Context, req *pb.UpdateCouponRequest) (*pb.UpdateCouponReply, error) {

	coupon := dm.Coupon{
		ID:          req.CouponID,
		Name:        req.Name,
		Photo:       req.Photo,
		Description: req.Description,
		Point:       req.Point,
		CategoryID:  req.CategoryID,
		Condition:   req.Condition,
	}

	err := d.usecase.Coupon.Update(coupon)

	if err != nil {
		return &pb.UpdateCouponReply{Success: false}, err
	}

	return &pb.UpdateCouponReply{Success: true}, nil
}

// GetCoupons method
func (d *RedeemDelivery) GetCoupons(ctx context.Context, req *pb.GetCouponsRequest) (*pb.GetCouponsReply, error) {

	coupons, err := d.usecase.Coupon.Fetch(int(req.Offset), int(req.Limit))

	if err != nil {
		return &pb.GetCouponsReply{}, err
	}

	var couponReplies = []*pb.Coupon{}

	for _, coupon := range coupons {

		couponReplies = append(couponReplies, &pb.Coupon{
			Id:          coupon.ID,
			Name:        coupon.Name,
			Photo:       coupon.Photo,
			Description: coupon.Description,
			Point:       coupon.Point,
			Condition:   coupon.Condition,
			Expire:      coupon.Expire.String(),
			Status:      coupon.Status,
			CategoryID:  coupon.CategoryID,
			StoreID:     coupon.StoreID,
		})
	}

	rsp := &pb.GetCouponsReply{
		Coupons: couponReplies,
	}

	// No error in this case
	return rsp, nil
}

// GetApproveCoupons method
func (d *RedeemDelivery) GetApproveCoupons(ctx context.Context, req *pb.GetApproveCouponsRequest) (*pb.GetApproveCouponsReply, error) {
	// Create Account
	coupons, err := d.usecase.Coupon.GetByStatus("active", int(req.Offset), int(req.Limit))

	if err != nil {
		return &pb.GetApproveCouponsReply{}, err
	}

	var couponReplies = []*pb.Coupon{}

	for _, coupon := range coupons {
		couponReplies = append(couponReplies, &pb.Coupon{
			Id:          coupon.ID,
			Name:        coupon.Name,
			Photo:       coupon.Photo,
			Description: coupon.Description,
			Point:       coupon.Point,
			Condition:   coupon.Condition,
			Expire:      coupon.Expire.String(),
			Status:      coupon.Status,
			CategoryID:  coupon.CategoryID,
			StoreID:     coupon.StoreID,
		})
	}

	rsp := &pb.GetApproveCouponsReply{
		Coupons: couponReplies,
	}

	// No error in this case
	return rsp, nil
}

// GetCouponsByStore method
func (d *RedeemDelivery) GetCouponsByStore(ctx context.Context, req *pb.GetCouponsByStoreRequest) (*pb.GetCouponsByStoreReply, error) {
	// Create Account
	coupons, err := d.usecase.Coupon.GetByStoreID(req.StoreID, int(req.Offset), int(req.Limit))

	if err != nil {
		return &pb.GetCouponsByStoreReply{}, err
	}

	var couponReplies = []*pb.Coupon{}

	for _, coupon := range coupons {
		couponReplies = append(couponReplies, &pb.Coupon{
			Id:          coupon.ID,
			Name:        coupon.Name,
			Photo:       coupon.Photo,
			Description: coupon.Description,
			Point:       coupon.Point,
			Condition:   coupon.Condition,
			Expire:      coupon.Expire.String(),
			Status:      coupon.Status,
			CategoryID:  coupon.CategoryID,
			StoreID:     coupon.StoreID,
		})
	}

	rsp := &pb.GetCouponsByStoreReply{
		Coupons: couponReplies,
	}

	// No error in this case
	return rsp, nil
}

// GetCouponsByCategory method
func (d *RedeemDelivery) GetCouponsByCategory(ctx context.Context, req *pb.GetCouponsByCategoryRequest) (*pb.GetCouponsByCategoryReply, error) {
	// Create Account
	coupons, err := d.usecase.Coupon.GetByCategoryID(req.CategoryID, int(req.Offset), int(req.Limit))

	if err != nil {
		return &pb.GetCouponsByCategoryReply{}, err
	}

	var couponReplies = []*pb.Coupon{}

	for _, coupon := range coupons {
		couponReplies = append(couponReplies, &pb.Coupon{
			Id:          coupon.ID,
			Name:        coupon.Name,
			Photo:       coupon.Photo,
			Description: coupon.Description,
			Point:       coupon.Point,
			Condition:   coupon.Condition,
			Expire:      coupon.Expire.String(),
			Status:      coupon.Status,
			CategoryID:  coupon.CategoryID,
			StoreID:     coupon.StoreID,
		})
	}

	rsp := &pb.GetCouponsByCategoryReply{
		Coupons: couponReplies,
	}

	// No error in this case
	return rsp, nil
}

// GetCouponsByKeyword method
func (d *RedeemDelivery) GetCouponsByKeyword(ctx context.Context, req *pb.GetCouponsByKeywordRequest) (*pb.GetCouponsByKeywordReply, error) {
	// Create Account
	coupons, err := d.usecase.Coupon.GetByKeyword(req.Keyword, int(req.Offset), int(req.Limit))

	if err != nil {
		return &pb.GetCouponsByKeywordReply{}, err
	}

	var couponReplies = []*pb.Coupon{}

	for _, coupon := range coupons {
		couponReplies = append(couponReplies, &pb.Coupon{
			Id:          coupon.ID,
			Name:        coupon.Name,
			Photo:       coupon.Photo,
			Description: coupon.Description,
			Point:       coupon.Point,
			Condition:   coupon.Condition,
			Expire:      coupon.Expire.String(),
			Status:      coupon.Status,
			CategoryID:  coupon.CategoryID,
			StoreID:     coupon.StoreID,
		})
	}

	rsp := &pb.GetCouponsByKeywordReply{
		Coupons: couponReplies,
	}

	// No error in this case
	return rsp, nil
}

// GetApproveCouponByStore method
func (d *RedeemDelivery) GetApproveCouponByStore(ctx context.Context, req *pb.GetApproveCouponByStoreRequest) (*pb.GetApproveCouponByStoreReply, error) {

	coupons, err := d.usecase.Coupon.GetByStoreIDAndStatus(req.StoreID, "active", int(req.Offset), int(req.Limit))

	if err != nil {
		return &pb.GetApproveCouponByStoreReply{}, err
	}

	var couponReplies []*pb.Coupon

	for _, coupon := range coupons {

		couponReplies = append(couponReplies, &pb.Coupon{
			Id:          coupon.ID,
			Name:        coupon.Name,
			Photo:       coupon.Photo,
			Description: coupon.Description,
			Point:       coupon.Point,
			Condition:   coupon.Condition,
			Expire:      coupon.Expire.String(),
			Status:      coupon.Status,
			StoreID:     coupon.StoreID,
			CategoryID:  coupon.CategoryID,
		})
	}

	rsp := &pb.GetApproveCouponByStoreReply{
		Coupons: couponReplies,
	}

	// No error in this case
	return rsp, nil
}

// GetCoupon method
func (d *RedeemDelivery) GetCoupon(ctx context.Context, req *pb.GetCouponRequest) (*pb.GetCouponReply, error) {
	// Create Account
	coupon, err := d.usecase.Coupon.GetByID(req.CouponID)

	if err != nil {
		return &pb.GetCouponReply{}, err
	}

	couponReply := &pb.Coupon{
		Id:          coupon.ID,
		Name:        coupon.Name,
		Photo:       coupon.Photo,
		Description: coupon.Description,
		Point:       coupon.Point,
		Condition:   coupon.Condition,
		Expire:      coupon.Expire.String(),
		Status:      coupon.Status,
		CategoryID:  coupon.CategoryID,
		StoreID:     coupon.StoreID,
	}

	// No error in this case
	return &pb.GetCouponReply{
		Coupon: couponReply,
	}, nil
}

// DeleteCoupon method
func (d *RedeemDelivery) DeleteCoupon(ctx context.Context, req *pb.DeleteCouponRequest) (*pb.DeleteCouponReply, error) {
	// Create Account
	err := d.usecase.Coupon.Delete(req.CouponID)

	if err != nil {
		return &pb.DeleteCouponReply{Success: false}, err
	}

	// No error in this case
	return &pb.DeleteCouponReply{Success: true}, nil
}

// ApproveCoupon method
func (d *RedeemDelivery) ApproveCoupon(ctx context.Context, req *pb.ApproveCouponRequest) (*pb.ApproveCouponReply, error) {
	// Create Account
	err := d.usecase.Coupon.Approve(req.CouponID)

	if err != nil {
		return &pb.ApproveCouponReply{Success: false}, err
	}

	// No error in this case
	return &pb.ApproveCouponReply{Success: true}, nil
}

// RejectCoupon method
func (d *RedeemDelivery) RejectCoupon(ctx context.Context, req *pb.RejectCouponRequest) (*pb.RejectCouponReply, error) {
	// Create Account
	err := d.usecase.Coupon.Reject(req.CouponID)

	if err != nil {
		return &pb.RejectCouponReply{Success: false}, err
	}

	// No error in this case
	return &pb.RejectCouponReply{Success: true}, nil
}
