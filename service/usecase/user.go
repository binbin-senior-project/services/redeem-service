package usecase

import (
	"errors"
	"fmt"

	amqp "gitlab.com/mangbinbin/services/redeem-service/service/delivery/amqp"
	dm "gitlab.com/mangbinbin/services/redeem-service/service/domain"
	r "gitlab.com/mangbinbin/services/redeem-service/service/repository"
)

// IUserUsecase interface
type IUserUsecase interface {
	Store(user dm.User) error
	GetByID(id string) (dm.User, error)
	Redeem(redeem dm.Redeem) error
	GetFavoritesByUserID(id string, offet int, limit int) ([]dm.Coupon, error)
	CheckFavoriteCoupon(id string, couponID string) (bool, error)
	FavoriteCoupon(id string, couponID string) error
	UnFavoriteCoupon(id string, couponID string) error
	UpdatePoint(id string, point float64) error
}

// UserUsecase is an object
type UserUsecase struct {
	repo   r.RegisterRepository
	broker *amqp.AMQPPublish
}

// NewUserUsecase is an inital method
func NewUserUsecase(repo r.RegisterRepository, broker *amqp.AMQPPublish) IUserUsecase {
	return &UserUsecase{
		repo:   repo,
		broker: broker,
	}
}

// Store method
func (u *UserUsecase) Store(user dm.User) error {
	// Create Account and retrive User
	err := u.repo.User.Store(user)

	return err
}

// GetByID method
func (u *UserUsecase) GetByID(id string) (dm.User, error) {
	// Create Account and retrive User
	user, err := u.repo.User.GetByID(id)

	return user, err
}

// Redeem method
func (u *UserUsecase) Redeem(redeem dm.Redeem) error {

	coupon, err := u.repo.Coupon.GetByID(redeem.CouponID)

	if err != nil {
		return err
	}

	user, err := u.repo.User.GetByID(redeem.UserID)

	if err != nil {
		return err
	}

	if user.Point < coupon.Point {
		return errors.New("point is no enough!")
	}

	up := user.Point - coupon.Point

	err = u.repo.User.UpdateByKey(user.ID, "user_point", fmt.Sprintf("%.2f", up))

	if err != nil {
		return err
	}

	err = u.repo.Redeem.Store(redeem)

	if err != nil {
		return err
	}

	// Update point to other services
	go u.broker.PointUpdatedEvent(dm.PointUpdatedEvent{
		UserID: user.ID,
		Point:  up,
	})

	// CreateTransaction to user service
	go u.broker.CreateUserTransaction(dm.UserTransactionCreatedEvent{
		UserID:      user.ID,
		Type:        "redeem",
		Point:       coupon.Point,
		Description: "แลกใช้คูปอง " + coupon.Name,
	})

	// CreateNotification to notification service
	go u.broker.CreateNotification(dm.NotificationCreatedEvent{
		UserID: user.ID,
		Title:  "แลกใช้คูปอง",
		Body:   "คุณแลกใช้ " + coupon.Name + " จำนวน " + fmt.Sprintf("%.2f", coupon.Point) + " แต้ม",
	})

	return err
}

// GetFavoritesByUserID method
func (u *UserUsecase) GetFavoritesByUserID(id string, offset int, limit int) ([]dm.Coupon, error) {

	favorites, err := u.repo.Favorite.GetByUserID(id, offset, limit)

	if err != nil {
		return []dm.Coupon{}, err
	}

	var coupons []dm.Coupon

	for _, favorite := range favorites {

		coupon, err := u.repo.Coupon.GetByID(favorite.CouponID)

		if err != nil {
			return []dm.Coupon{}, err
		}

		coupons = append(coupons, coupon)
	}

	return coupons, nil
}

// CheckFavoriteCoupon method
func (u *UserUsecase) CheckFavoriteCoupon(id string, couponID string) (bool, error) {

	favorite, err := u.repo.Favorite.GetByUserIDAndCouponID(id, couponID)

	if err != nil {
		return false, err
	}

	if favorite == (dm.Favorite{}) {
		return false, nil
	}

	return true, nil
}

// FavoriteCoupon method
func (u *UserUsecase) FavoriteCoupon(id string, couponID string) error {

	err := u.repo.Favorite.Store(id, couponID)

	return err
}

// UnFavoriteCoupon method
func (u *UserUsecase) UnFavoriteCoupon(id string, couponID string) error {

	err := u.repo.Favorite.Delete(id, couponID)

	return err
}

// UpdatePoint method
func (u *UserUsecase) UpdatePoint(userID string, point float64) error {

	err := u.repo.User.UpdateByKey(userID, "user_point", fmt.Sprintf("%f", point))

	return err
}
