package usecase

import (
	amqp "gitlab.com/mangbinbin/services/redeem-service/service/delivery/amqp"
	dm "gitlab.com/mangbinbin/services/redeem-service/service/domain"
	r "gitlab.com/mangbinbin/services/redeem-service/service/repository"
)

// ICouponUsecase interface
type ICouponUsecase interface {
	Store(coupon dm.Coupon) error
	Fetch(offset int, limit int) ([]dm.Coupon, error)
	GetByKeyword(keyword string, offset int, limit int) ([]dm.Coupon, error)
	GetByCategoryID(categoryID string, offset int, limit int) ([]dm.Coupon, error)
	GetByStoreID(storeID string, offset int, limit int) ([]dm.Coupon, error)
	GetByStoreIDAndStatus(storeID string, status string, offset int, limit int) ([]dm.Coupon, error)
	GetByStatus(status string, offset int, limit int) ([]dm.Coupon, error)
	GetByID(id string) (dm.Coupon, error)
	Update(coupon dm.Coupon) error
	Delete(id string) error
	Approve(id string) error
	Reject(id string) error
}

// CouponUsecase is an object
type CouponUsecase struct {
	repo   r.RegisterRepository
	broker *amqp.AMQPPublish
}

// NewCouponUsecase is an inital method for TrashRepository struct
// Called by Client (main.go)
func NewCouponUsecase(repo r.RegisterRepository, broker *amqp.AMQPPublish) ICouponUsecase {
	return &CouponUsecase{
		repo:   repo,
		broker: broker,
	}
}

// Store method
func (u *CouponUsecase) Store(coupon dm.Coupon) error {

	err := u.repo.Coupon.Store(coupon)

	// CreateTransaction Store
	go u.broker.CreateStoreTransaction(dm.StoreTransactionCreatedEvent{
		StoreID:     coupon.StoreID,
		Description: "ส่งคำขอสร้างคูปอง",
	})

	// CreateNotification to notification service
	go u.broker.CreateNotification(dm.NotificationCreatedEvent{
		UserID: coupon.UserID,
		Title:  "ส่งคำขออนุมัติคูปอง",
		Body:   "ส่งคำขออนุมัติคูปองเรียบร้อยแล้ว",
	})

	return err
}

// Fetch method
func (u *CouponUsecase) Fetch(offset int, limit int) ([]dm.Coupon, error) {

	coupons, err := u.repo.Coupon.Fetch(offset, limit)

	return coupons, err
}

// GetByKeyword method
func (u *CouponUsecase) GetByKeyword(keyword string, offset int, limit int) ([]dm.Coupon, error) {

	coupons, err := u.repo.Coupon.GetByKeyword(keyword, offset, limit)

	return coupons, err
}

// GetByCategoryID method
func (u *CouponUsecase) GetByCategoryID(categoryID string, offset int, limit int) ([]dm.Coupon, error) {

	coupons, err := u.repo.Coupon.GetByCategoryID(categoryID, offset, limit)

	return coupons, err
}

// GetByStoreID method
func (u *CouponUsecase) GetByStoreID(storeID string, offset int, limit int) ([]dm.Coupon, error) {

	coupons, err := u.repo.Coupon.GetByStoreID(storeID, offset, limit)

	return coupons, err
}

// GetByStoreIDAndStatus method
func (u *CouponUsecase) GetByStoreIDAndStatus(storeID string, status string, offset int, limit int) ([]dm.Coupon, error) {

	coupons, err := u.repo.Coupon.GetByStoreIDAndStatus(storeID, status, offset, limit)

	return coupons, err
}

// GetByStatus method
func (u *CouponUsecase) GetByStatus(status string, offset int, limit int) ([]dm.Coupon, error) {

	coupons, err := u.repo.Coupon.GetByStatus(status, offset, limit)

	return coupons, err
}

// GetByID method
func (u *CouponUsecase) GetByID(id string) (dm.Coupon, error) {

	coupon, err := u.repo.Coupon.GetByID(id)

	return coupon, err
}

// Update method
func (u *CouponUsecase) Update(coupon dm.Coupon) error {

	err := u.repo.Coupon.Update(coupon)

	return err
}

// Delete method
func (u *CouponUsecase) Delete(id string) error {

	err := u.repo.Coupon.Delete(id)

	return err
}

// Approve method
func (u *CouponUsecase) Approve(id string) error {

	err := u.repo.Coupon.UpdateByKey(id, "coupon_status", "active")

	coupon, _ := u.repo.Coupon.GetByID(id)

	// CreateTransaction to store service
	go u.broker.CreateStoreTransaction(dm.StoreTransactionCreatedEvent{
		StoreID:     coupon.StoreID,
		Description: "คูปอง " + coupon.Name + " ได้รับการอนุมัติ",
	})

	// CreateNotification to notification service
	go u.broker.CreateNotification(dm.NotificationCreatedEvent{
		UserID: coupon.UserID,
		Title:  "คูปองยืนยันเรียบร้อย",
		Body:   "คูปอง " + coupon.Name + " ได้รับการอนุมัติเรียบร้อยแล้ว",
	})

	return err
}

// Reject method
func (u *CouponUsecase) Reject(id string) error {

	err := u.repo.Coupon.UpdateByKey(id, "coupon_status", "rejected")

	coupon, _ := u.repo.Coupon.GetByID(id)

	// CreateTransaction to store service
	go u.broker.CreateStoreTransaction(dm.StoreTransactionCreatedEvent{
		StoreID:     coupon.StoreID,
		Description: "คูปอง" + coupon.Name + "ไม่ผ่านการอนุมัติ",
	})

	// CreateNotification to notification service
	go u.broker.CreateNotification(dm.NotificationCreatedEvent{
		UserID: coupon.UserID,
		Title:  "คูปองยืนยันเรียบร้อย",
		Body:   "คูปอง" + coupon.Name + "ไม่ผ่านการอนุมัติ",
	})

	return err
}
