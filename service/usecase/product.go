package usecase

import (
	amqp "gitlab.com/mangbinbin/services/redeem-service/service/delivery/amqp"
	dm "gitlab.com/mangbinbin/services/redeem-service/service/domain"
	r "gitlab.com/mangbinbin/services/redeem-service/service/repository"
)

// IProductUsecase interface
type IProductUsecase interface {
	Store(product dm.Product) error
	Fetch(offset int, limit int) ([]dm.Product, error)
	GetByID(id string) (dm.Product, error)
	Update(product dm.Product) error
	Delete(id string) error
}

// ProductUsecase is an object
type ProductUsecase struct {
	repo   r.RegisterRepository
	broker *amqp.AMQPPublish
}

// NewProductUsecase is an inital method for TrashRepository struct
// Called by Client (main.go)
func NewProductUsecase(repo r.RegisterRepository, broker *amqp.AMQPPublish) IProductUsecase {
	return &ProductUsecase{
		repo:   repo,
		broker: broker,
	}
}

// Store method is a method
func (r *ProductUsecase) Store(product dm.Product) error {

	err := r.repo.Product.Store(product)

	return err
}

// Fetch method is a method
func (r *ProductUsecase) Fetch(offset int, limit int) ([]dm.Product, error) {

	products, err := r.repo.Product.Fetch(offset, limit)

	return products, err
}

// GetByID method is a method
func (r *ProductUsecase) GetByID(id string) (dm.Product, error) {

	product, err := r.repo.Product.GetByID(id)

	return product, err
}

// Update is a method
func (r *ProductUsecase) Update(product dm.Product) error {

	err := r.repo.Product.Update(product)

	return err
}

// Delete is a method
func (r *ProductUsecase) Delete(id string) error {

	err := r.repo.Product.Delete(id)

	return err
}
