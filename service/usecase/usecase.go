package usecase

// RegisterUsecase struct
type RegisterUsecase struct {
	Analytic IAnalyticUsecase
	Boost    IBoostUsecase
	Category ICategoryUsecase
	Coupon   ICouponUsecase
	Product  IProductUsecase
	User     IUserUsecase
}
