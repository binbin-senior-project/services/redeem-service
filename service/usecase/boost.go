package usecase

import (
	"time"

	amqp "gitlab.com/mangbinbin/services/redeem-service/service/delivery/amqp"
	dm "gitlab.com/mangbinbin/services/redeem-service/service/domain"
	r "gitlab.com/mangbinbin/services/redeem-service/service/repository"
)

// IBoostUsecase interface
type IBoostUsecase interface {
	Store(couponIDs []string, productID string) error
	Fetch(offset int, limit int) ([]dm.BoostItem, error)
	Approve(id string) error
	Reject(id string) error
}

// BoostUsecase is an object
type BoostUsecase struct {
	repo   r.RegisterRepository
	broker *amqp.AMQPPublish
}

// NewBoostUsecase is an inital method for TrashRepository struct
// Called by Client (main.go)
func NewBoostUsecase(repo r.RegisterRepository, broker *amqp.AMQPPublish) IBoostUsecase {
	return &BoostUsecase{
		repo:   repo,
		broker: broker,
	}
}

// Store method
func (u *BoostUsecase) Store(couponIDs []string, productID string) error {

	for _, couponID := range couponIDs {
		err := u.repo.Boost.Store(couponID, productID)

		if err != nil {
			return err
		}
	}

	// CreateTransaction Store
	coupon, _ := u.repo.Coupon.GetByID(couponIDs[0])

	go u.broker.CreateStoreTransaction(dm.StoreTransactionCreatedEvent{
		StoreID:     coupon.StoreID,
		Description: "ส่งคำขอบูสต์คูปอง",
	})

	// CreateNotification to notification service
	go u.broker.CreateNotification(dm.NotificationCreatedEvent{
		UserID: coupon.UserID,
		Title:  "ส่งคำขอบูสต์คูปอง",
		Body:   "ส่งคำขอบูสต์คูปองเรียบร้อยแล้ว",
	})

	return nil
}

// Fetch method
func (u *BoostUsecase) Fetch(offset int, limit int) ([]dm.BoostItem, error) {

	boosts, err := u.repo.Boost.Fetch(offset, limit)

	if err != nil {
		return []dm.BoostItem{}, err
	}

	var boostItems []dm.BoostItem

	for _, boost := range boosts {
		coupon, err := u.repo.Coupon.GetByID(boost.CouponID)

		if err != nil {
			return []dm.BoostItem{}, err
		}

		product, err := u.repo.Product.GetByID(boost.ProductID)

		if err != nil {
			return []dm.BoostItem{}, err
		}

		boostItems = append(boostItems, dm.BoostItem{
			ID:          boost.ID,
			CouponName:  coupon.Name,
			ProductName: product.Name,
		})
	}

	return boostItems, nil
}

// Approve method
func (u *BoostUsecase) Approve(id string) error {

	boost, err := u.repo.Boost.GetByID(id)

	if err != nil {
		return err
	}

	coupon, err := u.repo.Coupon.GetByID(boost.CouponID)

	if err != nil {
		return err
	}

	product, err := u.repo.Product.GetByID(boost.ProductID)

	if err != nil {
		return err
	}

	days := coupon.Expire.Add(time.Hour * 24 * time.Duration(product.Day))

	err = u.repo.Coupon.UpdateExpire(coupon.ID, days)

	if err != nil {
		return err
	}

	err = u.repo.Boost.Delete(id)

	if err != nil {
		return err
	}

	// CreateTransaction Store
	go u.broker.CreateStoreTransaction(dm.StoreTransactionCreatedEvent{
		StoreID:     coupon.StoreID,
		Description: "คูปอง" + coupon.Name + "ได้รับการบูสต์",
	})

	// CreateNotification to notification service
	go u.broker.CreateNotification(dm.NotificationCreatedEvent{
		UserID: coupon.UserID,
		Title:  "คูปองยืนยันเรียบร้อย",
		Body:   "คูปอง" + coupon.Name + "ได้รับการขยายเวลาเรียบร้อยแล้ว",
	})

	return nil
}

// Reject method
func (u *BoostUsecase) Reject(id string) error {

	err := u.repo.Boost.Delete(id)

	boost, _ := u.repo.Boost.GetByID(id)
	coupon, _ := u.repo.Coupon.GetByID(boost.CouponID)

	// CreateTransaction Store
	go u.broker.CreateStoreTransaction(dm.StoreTransactionCreatedEvent{
		StoreID:     coupon.StoreID,
		Description: "คูปอง" + coupon.Name + "ไม่ผ่านเงื่อนไขการบูสต์",
	})

	// CreateNotification to notification service
	go u.broker.CreateNotification(dm.NotificationCreatedEvent{
		UserID: coupon.UserID,
		Title:  "คูปองยืนยันเรียบร้อย",
		Body:   "คูปอง" + coupon.Name + "ไม่ผ่านเงื่อนไขการบูสต์",
	})

	return err
}
