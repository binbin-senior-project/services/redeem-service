package usecase

import (
	amqp "gitlab.com/mangbinbin/services/redeem-service/service/delivery/amqp"
	r "gitlab.com/mangbinbin/services/redeem-service/service/repository"
)

// IAnalyticUsecase interface
type IAnalyticUsecase interface {
	GetNewCouponToday() (int, error)
	GetRedeems(couponID string) (int, error)
}

// AnalyticUsecase is an object
type AnalyticUsecase struct {
	repo   r.RegisterRepository
	broker *amqp.AMQPPublish
}

// NewAnalyticUsecase is an inital method
func NewAnalyticUsecase(repo r.RegisterRepository, broker *amqp.AMQPPublish) IAnalyticUsecase {
	return &AnalyticUsecase{
		repo:   repo,
		broker: broker,
	}
}

// GetNewCouponToday method
func (u *AnalyticUsecase) GetNewCouponToday() (int, error) {
	count, err := u.repo.Analytic.GetNewCouponToday()

	return count, err
}

// GetRedeems method
func (u *AnalyticUsecase) GetRedeems(couponID string) (int, error) {
	count, err := u.repo.Analytic.GetRedeems(couponID)

	return count, err
}
