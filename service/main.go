package main

import (
	"github.com/joho/godotenv"

	db "gitlab.com/mangbinbin/services/redeem-service/service/database"
	amqp "gitlab.com/mangbinbin/services/redeem-service/service/delivery/amqp"
	grpc "gitlab.com/mangbinbin/services/redeem-service/service/delivery/grpc"
	r "gitlab.com/mangbinbin/services/redeem-service/service/repository"
	u "gitlab.com/mangbinbin/services/redeem-service/service/usecase"

	_ "github.com/lib/pq"
)

func main() {
	// For development purpose
	godotenv.Load()
	database := db.NewConnectDatabase()

	repo := r.RegisterRepository{
		Analytic: r.NewAnalyticRepository(database),
		Boost:    r.NewBoostRepository(database),
		Category: r.NewCategoryRepository(database),
		Coupon:   r.NewCouponRepository(database),
		Favorite: r.NewFavoriteRepository(database),
		Product:  r.NewProductRepository(database),
		Redeem:   r.NewRedeemRepository(database),
		User:     r.NewUserRepository(database),
	}

	// Register Broker Client
	ch := amqp.NewRedeemDelivery()
	// Register Publisher broker
	broker := amqp.NewAMQPPublish(ch)
	// Register Subscriber broker
	amqp.NewAMQPSubscribe(repo, ch)

	usecase := u.RegisterUsecase{
		Analytic: u.NewAnalyticUsecase(repo, broker),
		Boost:    u.NewBoostUsecase(repo, broker),
		Category: u.NewCategoryUsecase(repo, broker),
		Coupon:   u.NewCouponUsecase(repo, broker),
		Product:  u.NewProductUsecase(repo, broker),
		User:     u.NewUserUsecase(repo, broker),
	}

	// Register GRPC Server
	grpc.NewRedeemDelivery(usecase)
}
